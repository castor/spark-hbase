name := "castor-spark-hbase"
version := "1.0"
organization := "ch.cern"

scalaVersion := "2.10.6"

val hadoopVersion = "2.6.0-cdh5.7.0"
val sparkVersion  = "1.6.0"
val hbaseVersion  = "1.2.0-cdh5.7.0"

scalacOptions ++= Seq("-deprecation")
javacOptions ++= Seq("-source", "1.7", "-target", "1.7")

libraryDependencies ++= Seq(
  "org.apache.spark"  %% "spark-core"            % sparkVersion  % "provided",
  "org.apache.spark"  %% "spark-streaming"       % sparkVersion  % "provided",
  "org.apache.spark"  %% "spark-streaming-kafka" % sparkVersion,
  "org.apache.hadoop" %  "hadoop-client"         % hadoopVersion % "provided",
  "org.apache.hbase"  %  "hbase-common"          % hbaseVersion,
  "org.apache.hbase"  %  "hbase-server"          % hbaseVersion,
  "org.apache.hbase"  %  "hbase-protocol"        % hbaseVersion,
  "org.apache.hbase"  %  "hbase-client"          % hbaseVersion,
  "eu.unicredit"      %% "hbase-rdd"             % "0.7.1",
  "org.json4s"        %% "json4s-native"         % "3.2.10",
  "com.github.scopt"  %% "scopt"                 % "3.4.0"
)

/* Exclude jars that conflict with Spark (see https://github.com/sbt/sbt-assembly)
libraryDependencies ~= { _ map {
  case m if Seq("org.apache.spark", "org.apache.hadoop").contains(m.organization) =>
    m.exclude("commons-logging", "commons-logging").
      exclude("commons-collections", "commons-collections").
      exclude("commons-beanutils", "commons-beanutils-core").
      exclude("com.esotericsoftware.minlog", "minlog").
      exclude("com.google.guava", "guava").
      exclude("org.apache.hadoop", "hadoop-yarn-api").
      exclude("org.mortbay.jetty", "servlet-api")

  case m => m
}}
*/

// Remove stub classes
assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

resolvers ++= Seq(
  "Spray Repository" at "http://repo.spray.cc/",
  "Cloudera Repository" at "https://repository.cloudera.com/artifactory/cloudera-repos/",
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Second Typesafe repo" at "http://repo.typesafe.com/typesafe/maven-releases/",
  "Mesosphere Public Repository" at "http://downloads.mesosphere.io/maven",
  Resolver.sonatypeRepo("public")
)
resolvers += Resolver.url("bintray-sbt-plugins", url("http://dl.bintray.com/sbt/sbt-plugin-releases"))(Resolver.ivyStylePatterns)
