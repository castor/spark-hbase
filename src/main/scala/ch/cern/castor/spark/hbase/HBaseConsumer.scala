package ch.cern.castor.spark.hbase

import scala.util.Try

import org.apache.log4j.{Logger, Level => LogLevel}


import org.apache.spark._
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming._
import org.apache.spark.streaming.kafka._

import scopt.OptionParser

import org.json4s._
import org.json4s.native.JsonMethods._
import org.json4s.native.Serialization

import _root_.kafka.serializer.{DefaultDecoder, StringDecoder}


object HBaseConsumer {

  val APPLICATION_NAME = "Castor HBase writer"

  case class Config(
    kafkaZookeeperUrl: String = null,
    kafkaTopics: String = null,
    kafkaConsumerGroup: String = "HBaseConsumer",
    kafkaConsumerThreads: Int = 2,
    kafkaOffsetReset: String = "largest",
    kafkaOffsetStorage: String = "kafka",
    kafkaDualCommit: String = "true",
    kafkaAutoCommitInterval: Int = 10,

    krbPrincipal: String = null,
    krbKeytab: String = null,

    hbaseZookeeperUrlList: String = null,
    hbaseColFamily: String = "f",
    hbaseTableReq: String = "castor:prod_req",
    hbaseTableFileID: String = "castor:prod_file",
    hbaseTableFilename: String = "castor:prod_filename",

    batchDuration: Int = 10,
    verbose: Boolean = false,
    debug: Boolean = false
  )

  private def _parseConfig(args: Array[String]): Config = {
    val parser = new scopt.OptionParser[Config](APPLICATION_NAME) {
      head(APPLICATION_NAME)
      opt[String]("kafka-zookeeper-url").required().action{ (x, c) =>
        c.copy(kafkaZookeeperUrl = x) }.text("Kafka Zookeeper URL")
      opt[String]("hbase-zookeeper-url-list").required().action{ (x, c) =>
        c.copy(hbaseZookeeperUrlList = x) }.text("HBase Zookeeper URL list")
      opt[String]("kafka-topics").required().action{ (x, c) =>
        c.copy(kafkaTopics = x) }.text("Kafka Topics to read (comma separated)")
      opt[String]("krb-principal").required().action{ (x, c) =>
        c.copy(krbPrincipal = x) }.text("Kerberos principal")
      opt[String]("krb-keytab").required().action{ (x, c) =>
        c.copy(krbKeytab = x) }.text("Keytab file to use")

      opt[Int]("kafka-consumer-threads").action{ (x, c) =>
        c.copy(kafkaConsumerThreads= x) }.text("Number of threads consuming from Kafka")
      opt[Int]("batch-duration").optional().action{ (x, c) =>
        c.copy(batchDuration = x) }.text("Spark batch duration")

      opt[Boolean]("verbose").action( (_, c) =>
        c.copy(verbose = true) ).text("Enable INFO logging level")
      opt[Boolean]("debug").action( (_, c) =>
        c.copy(debug = true) ).text("Enable DEBUG logging level")
      help("help").text("prints this usage text")
    }

    // parser.parse returns Option[Config]
    parser.parse(args, Config()) match {
      case Some(config) =>
        // do stuff
        config;
      case None =>
        // arguments are bad, error message will have been displayed
        System.exit(1)
        null
    }
  }

  private val keys_req      = Array("REQID")
  private val keys_fileId   = Array("NSFILEID")
  private val keys_fileName = Array("Filename", "lastKnownFileName", "Path")

  private val logger   = Logger.getLogger(this.getClass())
  Logger.getRootLogger.setLevel(LogLevel.ERROR)

  def forgeTimestamp(msg: Map[String, String]): String = {
      val first = msg.getOrElse("EPOCH", ((System.currentTimeMillis / 1000).toString))
      val last  = msg.getOrElse("USECS", "000000")
      (first + "." + last)
  }

  def saveToHBase(rdd: RDD[Map[String, String]], table: String, config: Config) {
    logger.info("Entering saveToHBase...,")
    import unicredit.spark.hbase._
    import org.apache.hadoop.security.UserGroupInformation
    import org.apache.hadoop.hbase.client.HBaseAdmin
    import org.apache.hadoop.hbase.HBaseConfiguration

    val hadoopConf = HBaseConfiguration.create()
    hadoopConf.set("dfs.namenode.kerberos.principal", "hdfs/_HOST@CERN.CH")
    hadoopConf.set("dfs.datanode.kerberos.principal", "hdfs/_HOST@CERN.CH")
    hadoopConf.set("hbase.zookeeper.quorum", config.hbaseZookeeperUrlList)
    hadoopConf.set("hbase.security.authorization", "true")
    hadoopConf.set("hbase.security.authentication", "kerberos")
    hadoopConf.set("hbase.master.kerberos.principal", "hbase/_HOST@CERN.CH")
    hadoopConf.set("hbase.regionserver.kerberos.principal", "hbase/_HOST@CERN.CH")

    implicit val hbaseConf = HBaseConfig(hadoopConf)

    UserGroupInformation.setConfiguration(hadoopConf)
    UserGroupInformation.loginUserFromKeytab(config.krbPrincipal, config.krbKeytab)
    HBaseAdmin.checkHBaseAvailable(hadoopConf)

    val keys = table match {
      case config.hbaseTableReq => keys_req
      case config.hbaseTableFileID => keys_fileId
      case config.hbaseTableFilename => keys_fileName
    }

    rdd.map( line => {
      implicit val formats = DefaultFormats
      val key = (keys collectFirst line).orNull
      val ts = forgeTimestamp(line)
      val jsonStr = Serialization.write(line)
      (key, Map(ts -> jsonStr))
    }).toHBase(table, config.hbaseColFamily)
    logger.info("Exiting saveToHBase...")
  }

  def main(args: Array[String]) {
		val config = _parseConfig(args)

    if (config.debug) Logger.getRootLogger.setLevel(LogLevel.DEBUG)
    if (config.verbose) Logger.getRootLogger.setLevel(LogLevel.INFO)

		val conf = new SparkConf()
			.setAppName("HBaseConsumer")
		val sc = new SparkContext(conf)
		val ssc = new StreamingContext(sc, Seconds(config.batchDuration))

		val kafkaParams = Map[String, String](
			"group.id"                -> config.kafkaConsumerGroup,
			"offsets.storage"         -> config.kafkaOffsetStorage,
			"auto.offset.reset"       -> config.kafkaOffsetReset,
			"zookeeper.connect"       -> config.kafkaZookeeperUrl,
			"dual.commit.enabled"     -> config.kafkaDualCommit,
			"auto.commit.interval.ms" -> config.kafkaAutoCommitInterval.toString
		)
		val topicMap = config.kafkaTopics.split(",").map((_, config.kafkaConsumerThreads)).toMap

		// Read from Kafka topics, persist to disk and only keep 2nd element of tuple2
		val stream = KafkaUtils.createStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicMap, StorageLevel.MEMORY_AND_DISK_SER_2)
			.map(_._2)

		// JSON Decode the "data" object (and throw the rest)
		val messages = stream
			.map((jsonMsg: String) => {
				implicit val formats = DefaultFormats
				try {
					logger.info(jsonMsg)
					(parse(jsonMsg) \ "data").values.asInstanceOf[Map[String, String]]
				}
			})

		// Check if message contains one of the keys and write to HBase
		//  (3 times)
		val lines_req = messages
			.filter( x => (keys_req collect x).nonEmpty)
			.foreachRDD( rdd => saveToHBase(rdd, config.hbaseTableReq, config))

		val lines_fileid = messages
			.filter( x => (keys_fileId collect x).nonEmpty)
			.foreachRDD( rdd => saveToHBase(rdd, config.hbaseTableFileID, config))

		val lines_filename = messages
			.filter( x => (keys_fileName collect x).nonEmpty)
			.foreachRDD( rdd => saveToHBase(rdd, config.hbaseTableFilename, config))


		// Start the Streaming job...
		ssc.start()
		ssc.awaitTermination()
  }
}
