# Spark based HBase producer for Castor

This container is meant to be run under some container scheduling system like Mesos.

Here is the configuration currently in use in production.

``` json
{
  "id": "/spark-castor-hbase2",
  "cmd": "/usr/lib/spark/bin/spark-submit --master local[*] --principal monitops@CERN.CH --keytab /etc/monit/monitops.keytab --conf spark.ui.port=$PORT0 --class ch.cern.castor.spark.hbase.HBaseConsumer /castormon/spark-hbase/castor-spark-hbase-assembly-1.0.jar  --kafka-zookeeper-url monit-zookeeper.cern.ch:2181/kafka/monit --kafka-topics castor_logs --krb-principal monitops@CERN.CH --krb-keytab /etc/monit/monitops.keytab --hbase-zookeeper-url-list p01001532965510.cern.ch,lxbse15c05.cern.ch,p05151113588139.cern.ch",
  "cpus": 0.5,
  "mem": 2048,
  "disk": 0,
  "instances": 1,
  "acceptedResourceRoles": [
    "*"
  ],
  "container": {
    "type": "DOCKER",
    "volumes": [
      {
        "containerPath": "/etc/hadoop/conf",
        "hostPath": "/etc/hadoop/conf",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/spark",
        "hostPath": "/usr/lib/spark",
        "mode": "RO"
      },
      {
        "containerPath": "/etc/monit",
        "hostPath": "/etc/monit",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/hadoop",
        "hostPath": "/usr/lib/hadoop",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/hadoop-mapreduce",
        "hostPath": "/usr/lib/hadoop-mapreduce",
        "mode": "RO"
      },
      {
        "containerPath": "/etc/spark/conf",
        "hostPath": "/etc/spark/conf",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/parquet/lib",
        "hostPath": "/usr/lib/parquet/lib",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/hadoop-yarn",
        "hostPath": "/usr/lib/hadoop-yarn",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/flume-ng",
        "hostPath": "/usr/lib/flume-ng",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/hadoop-hdfs/",
        "hostPath": "/usr/lib/hadoop-hdfs/",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/avro",
        "hostPath": "/usr/lib/avro",
        "mode": "RO"
      },
      {
        "containerPath": "/usr/lib/zookeeper",
        "hostPath": "/usr/lib/zookeeper",
        "mode": "RO"
      }
    ],
    "docker": {
      "image": "gitlab-registry.cern.ch/castor/spark-hbase:qa",
      "network": "HOST",
      "portMappings": [],
      "privileged": false,
      "parameters": [],
      "forcePullImage": true
    }
  },
  "healthChecks": [
    {
      "gracePeriodSeconds": 300,
      "intervalSeconds": 60,
      "timeoutSeconds": 20,
      "maxConsecutiveFailures": 3,
      "portIndex": 0,
      "path": "/api/v1/applications",
      "protocol": "HTTP",
      "ignoreHttp1xx": false
    }
  ],
  "portDefinitions": [
    {
      "port": 10002,
      "protocol": "tcp",
      "labels": {}
    }
  ],
  "uris": [
    "file:///etc/monit/docker-gitlab.tar.gz"
  ],
  "fetch": [
    {
      "uri": "file:///etc/monit/docker-gitlab.tar.gz",
      "extract": true,
      "executable": false,
      "cache": false
    }
  ]
}
```
